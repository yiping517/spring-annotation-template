package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 1.不實現Controller接口
 * 2.方法名不做要求
 * 3.返回值可以是ModelAndView，也可以是String
 * 4.可以添加多個方法，即一個Controller可以處理多個請求
 * 5.使用@Controller取代在spring配置文件中的配置。
 * 6.可以在方法前or類前加@RequestMapping。取代之前配置的HandlerMapping。
 */
@Controller
public class HelloController {
	
	/**
	 * http://ip:port/springmvc-annotation/hello.do
	 */
	@RequestMapping("/hello.do")
	public String hello() {
		System.out.println("hello");
		
		//返回視圖名
		return "hello";
	}

}
